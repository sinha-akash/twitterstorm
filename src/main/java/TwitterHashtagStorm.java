import java.util.*;

import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;

public class TwitterHashtagStorm {
   public static void main(String[] args) throws Exception{
      String consumerKey = "KCUl4nGPYoJea5pK5Sza1TJZF";
      String consumerSecret =  "ZDyfTFy7s0PqpCQIxsOJPLr4rbpg96x4DnyqGzQKj51lq3hU5y";
		
      String accessToken = "1001065514871939072-LmZKJI2rvCllxr4PFd69bGJ9TDBLpc";
      String accessTokenSecret =  "25klHkBmZJ9m4MmDhLvWmlJ4PTAXEuDn4OPvc1GSRRFrg";
		
     // String[] arguments = args.clone();
      String[] arguments = {"KCUl4nGPYoJea5pK5Sza1TJZF","ZDyfTFy7s0PqpCQIxsOJPLr4rbpg96x4DnyqGzQKj51lq3hU5y","1001065514871939072-LmZKJI2rvCllxr4PFd69bGJ9TDBLpc","25klHkBmZJ9m4MmDhLvWmlJ4PTAXEuDn4OPvc1GSRRFrg"};
      String[] keyWords = Arrays.copyOfRange(arguments, 4, arguments.length);
		
      Config config = new Config();
      config.setDebug(true);
		
      TopologyBuilder builder = new TopologyBuilder();
      builder.setSpout("twitter-spout", new TwitterSampleSpout(consumerKey,
         consumerSecret, accessToken, accessTokenSecret, keyWords));

      builder.setBolt("twitter-hashtag-reader-bolt", new HashtagReaderBolt())
         .shuffleGrouping("twitter-spout");

      builder.setBolt("twitter-hashtag-counter-bolt", new HashtagCounterBolt())
         .fieldsGrouping("twitter-hashtag-reader-bolt", new Fields("hashtag"));
      
      if (args != null && args.length > 0) {
          config.setNumWorkers(3);
          StormSubmitter.submitTopologyWithProgressBar(args[0], config, builder.createTopology());
        }
      else
      {
			
      LocalCluster cluster = new LocalCluster();
      cluster.submitTopology("TwitterHashtagStorm", config,
         builder.createTopology());
      Thread.sleep(50000);
      cluster.shutdown();
      }
   }
}
